
## Provisionning

 1. Need a public IP address
 2. Deploy Debian sid/buster
 3. Add a DNS entry like xxxx.machine.deuxfleurs.fr A 0.0.0.0 in Cloudflare + Havelock
 4. Setup the fqdn in /etc/hosts (127.0.1.1 xxxx.machine.deuxfleurs.fr)
 5. Switch the SSH port to the port 110
 6. Add the server to the ./production file 
 7. Reboot machine
 8. Deploy Ansible
 9. Check that everything works as intended
 10. Update NS 1.cluster.deuxfleurs.fr

## Useful commands

Show every variables collected by Ansible for a given host:

```
ansible -i production villequin.machine.deuxfleurs.fr -m setup
```

Run playbook for only one host:

```
ansible-playbook -i production --limit villequin.machine.deuxfleurs.fr site.yml
```

Dump hostvars:

```
ansible -m debug villequin.machine.deuxfleurs.fr -i ./production -a "var=hostvars"
```

Deploy only one tag:

```
ansible-playbook -i production site.yml --tags "container"
```

Redeploy everything:

```
ansible-playbook -i production site.yml
```

Upgrade packages and force overwirte to fix bad packing done by GlusterFS:

```
apt-get -o Dpkg::Options::="--force-overwrite"  dist-upgrade -y
```

