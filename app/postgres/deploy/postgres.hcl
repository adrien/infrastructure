job "postgres" {
  datacenters = ["dc1"]
  type = "system"
  priority = 90

  update {
    max_parallel     = 1
    stagger = "2m"
  }

  group "postgres" {
    network {
      port "psql_proxy_port" { static = 5432 }
      port "psql_port" { static = 5433 }
    }

    task "sentinel" {
      driver = "docker"

      config {
        image = "superboum/amd64_postgres:v3"
        network_mode = "host" 
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-sentinel"
        args = [
          "--cluster-name", "pissenlit",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
        ]
      }
      resources {
        memory = 100
      }
    }

    task "proxy" {
      driver = "docker"

      config {
        image = "superboum/amd64_postgres:v3"
        network_mode = "host" 
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-proxy"
        args = [
          "--cluster-name", "pissenlit",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
          "--port", "${NOMAD_PORT_psql_proxy_port}",
          "--listen-address", "0.0.0.0"
        ]
        ports = [ "psql_proxy_port" ]
      }

      resources {
        memory = 100
      }

      service {
        tags = ["sql"]
        port = "psql_proxy_port"
        address_mode = "host"
        name = "psql-proxy"
        check {
          type = "tcp"
          port = "psql_proxy_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "10m"
            ignore_warnings = false
          }
        }
      }
    }

    task "keeper" {
      driver = "docker"

      config {
        image = "superboum/amd64_postgres:v3"
        network_mode = "host" 
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-keeper"
        args = [
          "--cluster-name", "pissenlit",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
          "--data-dir", "/mnt/persist",
          "--pg-su-password", "${PG_SU_PWD}",
          "--pg-repl-username", "${PG_REPL_USER}",
          "--pg-repl-password", "${PG_REPL_PWD}",
          "--pg-listen-address", "${attr.unique.network.ip-address}",
          "--pg-port", "${NOMAD_PORT_psql_port}",
          "--pg-bin-path", "/usr/lib/postgresql/9.6/bin/"
        ]
        ports = [ "psql_port" ]
        volumes = [
          "/mnt/ssd/postgres:/mnt/persist"
        ]
      }

      template {
        data = file("../config/keeper/env.tpl")
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 500
      }

      service {
        tags = ["sql"]
        port = "psql_port"
        address_mode = "host"
        name = "keeper"
        check {
          type = "tcp"
          port = "psql_port"
          interval = "60s"
          timeout = "5s"

          check_restart {
            limit = 3
            grace = "60m"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

