job "core" {
  datacenters = ["dc1"]
  type = "system"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  update {
    max_parallel     = 1
    stagger = "1m"
  }

  group "network" {
    task "diplonat" {
      driver = "docker"

      config {
        image = "darkgallium/amd64_diplonat:v2"
        network_mode = "host"
        readonly_rootfs = true
	privileged = true
      }

      template {
        data = <<EOH
DIPLONAT_PRIVATE_IP={{ env "attr.unique.network.ip-address" }}
DIPLONAT_REFRESH_TIME=60
DIPLONAT_EXPIRATION_TIME=300
DIPLONAT_CONSUL_NODE_NAME={{ env "attr.unique.hostname" }}
RUST_LOG=debug
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 40
      }
    }
  }
}
