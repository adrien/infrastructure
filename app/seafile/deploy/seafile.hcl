job "seafile" {
  datacenters = ["dc1"]
  type = "service"
  priority = 10

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "main" {
    count = 1

    network {
      port "seafile-frontend_port" { static = 8000 }
      port "seafile-seafhttp_port" { static = 8083 }
      port "seafile-dav_port" { static = 8084 }
      port "seafile-hack_port" { static = 8085 } 
      port "mariadb_port" { static = 3306 }
    }

    task "mariadb" {
      driver = "docker"
      config {
        image = "superboum/amd64_mariadb:v4"
        network_mode = "host"
        command = "tail"
        ports = [ "mariadb_port" ]
        args = [
          "-f", "/var/log/mysql/error.log",
        ]
        volumes = [
          "/mnt/glusterfs/mariadb/main/server:/var/lib/mysql",
        ]
      }

      template {
        data = file("../config/mariadb/main/env.tpl")
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 800
      }

      service {
        tags = ["mariadb"]
        port = "mariadb_port"
        address_mode = "host"
        name = "mariadb"
        check {
          type = "tcp"
          port = "mariadb_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }


    task "hack" {
      driver = "docker"
      config {
        image = "alpine/socat:1.0.5"
        network_mode = "host"
        ports = [ "seafile-hack_port" ]
        command = "tcp6-listen:8085,fork,reuseaddr"
        args = [ "tcp-connect:127.0.0.1:8083" ]
      }
      resources {
        memory = 10
      }
      service {
        tags = [
          "seafile",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:cloud.deuxfleurs.fr;PathPrefixStrip:/seafhttp"

        ]
        port = "seafile-hack_port"
        address_mode = "host"
        name = "seafhttp"
        check {
          type = "tcp"
          port = "seafile-hack_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

    }
       
    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_seafile:v6"
        network_mode = "host"
        ports = [ "seafile-frontend_port", "seafile-dav_port", "seafile-seafhttp_port" ]
        
        ## cmd + args are used for running an instance attachable for update
        # command = "/bin/sleep"
        # args = ["999999"]

        mounts = [
          {
            type = "bind"
            source = "/mnt/glusterfs/seafile"
            target = "/mnt/seafile-data"
          }
        ]

        volumes = [
          "secrets/conf:/srv/webstore/conf",
          "secrets/ccnet:/srv/webstore/ccnet"
        ]
      }

      resources {
        memory = 600
      }

      service {
        tags = [
          "seafile",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:cloud.deuxfleurs.fr;PathPrefix:/"
        ]
        port = "seafile-frontend_port"
        address_mode = "host"
        name = "seahub"
        check {
          type = "tcp"
          port = "seafile-frontend_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = [
          "seafile",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:cloud.deuxfleurs.fr;PathPrefix:/seafdav"

        ]
        port = "seafile-dav_port"
        address_mode = "host"
        name = "seafdav"
        check {
          type = "tcp"
          port = "seafile-dav_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      template {
        data = file("../config/conf/ccnet.conf.tpl")
        destination = "secrets/conf/ccnet.conf"
      }

      template {
        data = file("../config/conf/seafile.conf.tpl")
        destination = "secrets/conf/seafile.conf"
      }

      template {
        data = file("../config/conf/seahub_settings.py.tpl")
        destination = "secrets/conf/seahub_settings.py"
      }

      template {
        data = file("../config/ccnet/seafile.ini")
        destination = "secrets/ccnet/seafile.ini"
      }
      template {
        data = file("../config/conf/seafdav.conf")
        destination = "secrets/conf/seafdav.conf"
      }
      template {
        data = file("../config/conf/gunicorn.conf")
        destination = "secrets/conf/gunicorn.conf"
      }

      # ---- secrets ----
      template {
        data = "{{ key \"secrets/seafile/conf/mykey.peer\" }}"
        destination = "secrets/ccnet/mykey.peer"
      }

      template {
        data = "{{ key \"secrets/seafile/conf/mykey.peer\" }}"
        destination = "secrets/conf/mykey.peer"
      }
    }
  }
}

