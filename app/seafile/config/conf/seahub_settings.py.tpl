SECRET_KEY = "8ep+sgi&s1-f2cq2178!ekk!0h0nw2y4z1-olbaopxmodsd8vk"
FILE_SERVER_ROOT = 'https://cloud.deuxfleurs.fr/seafhttp'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'seahub-db',
        'USER': 'seafile',
        'PASSWORD': '{{ key "secrets/seafile/ccnet/mysql_pwd" | trimSpace }}',
        'HOST': 'mariadb.service.2.cluster.deuxfleurs.fr',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': 'SET storage_engine=INNODB',
        }
    }
}
FILE_PREVIEW_MAX_SIZE = 100 * 1024 * 1024
ENABLE_THUMBNAIL = True
THUMBNAIL_ROOT = '/mnt/seafile-data/thumbnail/thumb/'
THUMBNAIL_EXTENSION = 'png'
THUMBNAIL_DEFAULT_SIZE = '24'
PREVIEW_DEFAULT_SIZE = '300'
