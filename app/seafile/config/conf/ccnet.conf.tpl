[General]
USER_NAME = deuxfleurs
ID = {{ key "secrets/seafile/ccnet/seafile_id" | trimSpace }}
NAME = deuxfleurs
SERVICE_URL = https://cloud.deuxfleurs.fr

[Network]
PORT = 10001

[Client]
PORT = 13418

[LDAP]
HOST = ldap://bottin2.service.2.cluster.deuxfleurs.fr/
BASE = ou=users,dc=deuxfleurs,dc=fr
USER_DN = {{ key "secrets/seafile/ccnet/ldap_binddn" | trimSpace }}
FILTER = memberOf=CN=seafile,OU=groups,DC=deuxfleurs,DC=fr
PASSWORD = {{ key "secrets/seafile/ccnet/ldap_bindpwd" | trimSpace }}
LOGIN_ATTR = mail

[Database]
ENGINE = mysql
HOST = mariadb.service.2.cluster.deuxfleurs.fr
PORT = 3306
USER = seafile
PASSWD = {{ key "secrets/seafile/ccnet/mysql_pwd" | trimSpace }}
DB = ccnet-db
CONNECTION_CHARSET = utf8

