[network]
port = 12001

[fileserver]
port = 8083
max_upload_size=8192
max_download_dir_size=8192

[database]
type = mysql
host = mariadb.service.2.cluster.deuxfleurs.fr
port = 3306
user = seafile
password = {{ key "secrets/seafile/ccnet/mysql_pwd" | trimSpace }}
db_name = seafile-db
connection_charset = utf8

[quota]
default = 50
