LDAP_URI = "ldap://bottin2.service.2.cluster.deuxfleurs.fr"
LDAP_BASE = "ou=users,dc=deuxfleurs,dc=fr"
LDAP_VERSION = 3
LDAP_BIND_DN = "{{ key "secrets/mariadb/main/ldap_binddn" | trimSpace }}"
LDAP_BIND_PW = "{{ key "secrets/mariadb/main/ldap_bindpwd" | trimSpace }}"
MYSQL_PASSWORD = "{{ key "secrets/mariadb/main/mysql_pwd" | trimSpace }}"
