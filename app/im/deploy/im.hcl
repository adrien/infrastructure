job "im" {
  datacenters = ["dc1"]
  type = "service"
  priority = 60

  group "matrix" {
    count = 1

    network {
      port "client_port" { static = 8008 }
      port "federation_port" { static = 8448 }
    }

    task "synapse" {
      driver = "docker"

      config {
        image = "particallydone/amd64_synapse:v41"
        network_mode = "host"
        readonly_rootfs = true
        ports = [ "client_port", "federation_port" ]
        command = "python"
        args = [
          "-m", "synapse.app.homeserver",
          "-n",
          "-c", "/etc/matrix-synapse/homeserver.yaml"
        ]
        volumes = [
          "secrets/conf:/etc/matrix-synapse",
          "/mnt/glusterfs/chat/matrix/synapse/media:/var/lib/matrix-synapse/media",
          "/mnt/glusterfs/chat/matrix/synapse/uploads:/var/lib/matrix-synapse/uploads",
          "/tmp/synapse-logs:/var/log/matrix-synapse",
          "/tmp/synapse:/tmp"
        ]
      }
      
      template {
        data = file("../config/synapse/homeserver.yaml")
        destination = "secrets/conf/homeserver.yaml"
      }

      template {
        data = file("../config/easybridge/registration.yaml.tpl")
        destination = "secrets/conf/easybridge_registration.yaml"
      }

      template {
        data = file("../config/synapse/log.yaml")
        destination = "secrets/conf/log.yaml"
      }

      template {
        data = file("../config/synapse/conf.d/server_name.yaml")
        destination = "secrets/conf/server_name.yaml"
      }

      template {
        data = file("../config/synapse/conf.d/report_stats.yaml")
        destination = "secrets/conf/report_stats.yaml"
      }

      # --- secrets ---
      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.crt\" }}"
        destination = "secrets/conf/homeserver.tls.crt"
      }

      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.dh\" }}"
        destination = "secrets/conf/homeserver.tls.dh"
      }

      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.key\" }}"
        destination = "secrets/conf/homeserver.tls.key"
      }

      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.signing.key\" }}"
        destination = "secrets/conf/homeserver.signing.key"
      }

      env {
        SYNAPSE_CACHE_FACTOR = 1
      }

      resources {
        cpu = 1000
        memory = 4000
      }

      service {
        name = "synapse-client"
        port = "client_port"
        address_mode = "host"
        tags = [
          "matrix",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:im.deuxfleurs.fr;PathPrefix:/_matrix",
          "traefik.frontend.headers.customResponseHeaders=Access-Control-Allow-Origin: *",
          "traefik.frontend.priority=100"
        ]
        check {
          type = "tcp"
          port = "client_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "synapse-federation"
        port = "federation_port"
        address_mode = "host"
        tags = [
          "matrix",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:deuxfleurs.fr;PathPrefix:/_matrix",
          "traefik.frontend.priority=100"
        ]
      }
    }
  }

  group "easybridge" {
    count = 1

    network {
      port "api_port" {
        static = 8321
        to = 8321
      }
      port "web_port" { to = 8281 }
    }

    task "easybridge" {
      driver = "docker"
      config {
        image = "lxpz/easybridge_amd64:33"
        ports = [ "api_port", "web_port" ]
        volumes = [
          "secrets/conf:/data"
        ]
        args = [ "./easybridge", "-config", "/data/config.json" ]
      }

      template {
        data = file("../config/easybridge/registration.yaml.tpl")
        destination = "secrets/conf/registration.yaml"
      }

      template {
        data = file("../config/easybridge/config.json.tpl")
        destination = "secrets/conf/config.json"
      }

      resources {
        memory = 500
        cpu = 1000
      }

      service {
        name = "easybridge-api"
        tags = ["easybridge-api"]
        port = "api_port"
        address_mode = "host"
        check {
          type = "tcp"
          port = "api_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "easybridge-web"
        tags = [
          "easybridge-web",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:easybridge.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
 }


  group "riotweb" {
    count = 1

    network {
      port "web_port" { to = 8043 }
    }

    task "server" {
      driver = "docker"
      config {
        image = "particallydone/amd64_riotweb:v20"
        ports = [ "web_port" ]
        volumes = [
          "secrets/config.json:/srv/http/config.json"
        ]
      }

      template {
        data = file("../config/riot_web/config.json")
        destination   = "secrets/config.json"
      }

      resources {
        memory = 21
      }

      service {
        tags = [
          "webstatic",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:im.deuxfleurs.fr,riot.deuxfleurs.fr;PathPrefix:/",
          "traefik.frontend.priority=10"
        ]
        port = "web_port"
        address_mode = "host"
        name = "webstatic"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

