job "frontend" {
  datacenters = ["dc1"]
  type = "service"
  priority = 80

  group "traefik" {
  
    network {
      port "http_port" { static = 80 }
      port "https_port" { static = 443 }
      port "admin_port" { static = 8082 }
    }

    task "server" {
      driver = "docker"

      config {
        image = "amd64/traefik:1.7.20"
        readonly_rootfs = true
        network_mode = "host"
        volumes = [
          "secrets/traefik.toml:/etc/traefik/traefik.toml",
        ]
        ports = [ "http_port", "https_port", "admin_port" ]
      }

      resources {
        memory = 265
      }

      template {
        data = file("../config/traefik.toml")
        destination = "secrets/traefik.toml"
      }

      service {
        name = "traefik-http"
        port = "http_port"
        tags = [ "(diplonat (tcp_port 80))" ]
        address_mode = "host"
      }

      service {
        name = "traefik-https"
        port = "https_port"
        tags = [ "(diplonat (tcp_port 443))" ]
        address_mode = "host"
      }

      service {
        name = "traefik-admin"
        port = "admin_port"
        address_mode = "host"
        check {
          type = "http"
          protocol = "http"
          port = 8082
          address_mode = "driver"
          path = "/ping"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

