job "plume" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "plume" {
    count = 1

    network {
      port "web_port" { }
    }

    task "plume" {
      driver = "docker"
      config {
        image = "superboum/plume:v2"
        network_mode = "host"
        ports = [ "web_port" ]
        #command = "cat"
        #args = [ "/dev/stdout" ]
        volumes = [
           "/mnt/glusterfs/plume/media:/app/static/media",
           "/mnt/glusterfs/plume/search:/app/search_index"
        ]
      }

      template {
        data = file("../config/app.env")
        destination = "secrets/app.env"
        env = true
      }

      resources {
        memory = 100
        cpu = 100
      }

      service {
        name = "plume"
        tags = [
          "plume",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:plume.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "http"
          protocol = "http"
          port = "web_port"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

