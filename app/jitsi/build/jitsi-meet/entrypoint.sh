#!/bin/bash

cat > /etc/nginx/sites-available/jitsi <<EOF
server_names_hash_bucket_size 64;

server {
    listen 0.0.0.0:${NGINX_PORT} ssl http2 default_server;
    listen [::]:${NGINX_PORT} ssl http2 default_server;
    server_name _;
    ssl_certificate ${JITSI_CERTS_FOLDER}/jitsi.deuxfleurs.fr.crt;
    ssl_certificate_key ${JITSI_CERTS_FOLDER}/jitsi.deuxfleurs.fr.key;
    root /srv/jitsi-meet;
    index index.html;
    location ~ ^/([a-zA-Z0-9=\?]+)$ {
        rewrite ^/(.*)$ / break;
    }
    location / {
        ssi on;
    }
    # BOSH, Bidirectional-streams Over Synchronous HTTP
    # https://en.wikipedia.org/wiki/BOSH_(protocol)
    location /http-bind {
        proxy_pass      http://${JITSI_PROSODY_BOSH_HOST}:${JITSI_PROSODY_BOSH_PORT}/http-bind;
        proxy_set_header X-Forwarded-For \$remote_addr;
        proxy_set_header Host \$http_host;
    }
    # external_api.js must be accessible from the root of the
    # installation for the electron version of Jitsi Meet to work
    # https://github.com/jitsi/jitsi-meet-electron
    location /external_api.js {
        alias /srv/jitsi-meet/libs/external_api.min.js;
    }
}
EOF

ln -sf /etc/nginx/sites-available/jitsi /etc/nginx/sites-enabled/jitsi

exec "$@"
