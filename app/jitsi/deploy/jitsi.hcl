job "jitsi" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "core" {

    network {
      port "bosh_port" { }
      port "ext_port" { static = 5347 }
      port "xmpp_port" { static = 5222 }
      port "https_port" { }
      port "video1_port" { static = 8081 }
      port "video2_port" { static = 10000 }
    }

    task "xmpp" {
      driver = "docker"
      config {
        image = "superboum/amd64_jitsi_xmpp:v8"
        ports = [ "bosh_port", "ext_port", "xmpp_port" ]
        network_mode = "host"
      }

      template {
        data = file("../config/global_env.tpl")
        destination = "secrets/global_env"
        env = true
      }

      # --- secrets ---
      template {
        data = "{{ key \"secrets/jitsi/auth.jitsi.deuxfleurs.fr.crt\" }}"
        destination = "secrets/certs/auth.jitsi.deuxfleurs.fr.crt"
      }

      template {
        data = "{{ key \"secrets/jitsi/auth.jitsi.deuxfleurs.fr.key\" }}"
        destination = "secrets/certs/auth.jitsi.deuxfleurs.fr.key"
      }

      template {
        data = "{{ key \"secrets/jitsi/jitsi.deuxfleurs.fr.crt\" }}"
        destination = "secrets/certs/jitsi.deuxfleurs.fr.crt"
      }

      template {
        data = "{{ key \"secrets/jitsi/jitsi.deuxfleurs.fr.key\" }}"
        destination = "secrets/certs/jitsi.deuxfleurs.fr.key"
      }

      resources {
        cpu = 300
        memory = 200
      }

      service {
        tags = [ "jitsi", "bosh" ]
        port = "bosh_port"
        address_mode = "host"
        name = "jitsi-xmpp-bosh"
        check {
          type = "tcp"
          port = "bosh_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = [ "jitsi", "ext" ]
        port = "ext_port"
        address_mode = "host"
        name = "jitsi-ext"
      }

      service {
        tags = [ "jitsi", "xmpp" ]
        port = "xmpp_port"
        address_mode = "host"
        name = "jitsi-xmpp"
      }
    }

    task "front" {
      driver = "docker"
      config {
        image = "superboum/amd64_jitsi_meet:v3"
        network_mode = "host"
        ports = [ "https_port" ]
      }

      template {
        data = file("../config/global_env.tpl")
        destination = "secrets/global_env"
        env = true
      }

      # --- secrets ---
      template {
        data = "{{ key \"secrets/jitsi/jitsi.deuxfleurs.fr.crt\" }}"
        destination = "secrets/certs/jitsi.deuxfleurs.fr.crt"
      }
      template {
        data = "{{ key \"secrets/jitsi/jitsi.deuxfleurs.fr.key\" }}"
        destination = "secrets/certs/jitsi.deuxfleurs.fr.key"
      }

      resources {
        cpu = 300
        memory = 200
      } 

     service {
        tags = [
          "jitsi",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:jitsi.deuxfleurs.fr;PathPrefix:/",
          "traefik.protocol=https"
        ]
        port = "https_port"
        address_mode = "host"
        name = "jitsi-front-https"
        check {
          type = "tcp"
          port = "https_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }

    task "jicofo" {
      driver = "docker"
      config {
        image = "superboum/amd64_jitsi_conference_focus:v6"
        network_mode = "host"
      }

      template {
        data = file("../config/global_env.tpl")
        destination = "secrets/global_env"
        env = true
      }

      #--- secrets ---
      template {
        data = "{{ key \"secrets/jitsi/jitsi.deuxfleurs.fr.crt\" }}"
        destination = "secrets/certs/jitsi.deuxfleurs.fr.crt"
      }

      template {
        data = "{{ key \"secrets/jitsi/auth.jitsi.deuxfleurs.fr.crt\" }}"
        destination = "secrets/certs/auth.jitsi.deuxfleurs.fr.crt"
      }

      resources {
        cpu = 300
        memory = 400
      } 
    }

    task "videobridge" {
      driver = "docker"
      config {
        image = "superboum/amd64_jitsi_videobridge:v16"
        network_mode = "host"
        ports = [ "video1_port", "video2_port" ] 
        ulimit {
          nofile = "1048576:1048576"
          nproc = "65536:65536"
        }
      }

      env {
        #JITSI_DEBUG = 1
        JITSI_VIDEO_TCP = 8081
        VIDEOBRIDGE_MAX_MEMORY = "1450m"
      }

      template {
        data = file("../config/global_env.tpl")
        destination = "secrets/global_env"
        env = true
      }

      resources {
        cpu = 900
        memory = 1500
      } 

      service {
        tags = [ "jitsi", "(diplonat (tcp_port 8081))" ]
        port = "video1_port"
        address_mode = "host"
        name = "jitsi-videobridge-video1"
        check {
          type = "tcp"
          port = "video1_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = [ "jitsi", "(diplonat (udp_port 10000))" ]
        port = "video2_port"
        address_mode = "host"
        name = "jitsi-videobridge-video2"
      }
    }
  }
}

