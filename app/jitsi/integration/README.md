## About Jitsi

Several server components:
  - prosody XMPP (ext)
  - jitsi videobridge aka JVB
  - jitsi conference focus aka jicofo aka focus
  - jitsi meet
  - octo
  - jigasi
  - jibri
  - etc.

Some libs:
  - libjitsi
  - jicoco
  - jitsi-utils
  - etc.

Client components:
  - jitsi meet electron
  - jitsi android/ios
  - etc.

## Conf

Base conf:

  - [videobridge.conf](https://github.com/jitsi/jitsi-videobridge/blob/master/jvb/src/main/resources/reference.conf)
  - [jicofo.conf](https://github.com/jitsi/jicofo/blob/master/src/main/resources/reference.conf)

the following is used in videobridge.conf:
[jicoco/MucClientConfiguration](https://github.com/jitsi/jicoco/blob/master/jicoco/src/main/java/org/jitsi/xmpp/mucclient/MucClientConfiguration.java)

How the new configuration is read in jicoco:
https://github.com/jitsi/jicoco/blob/master/jicoco-config/src/main/kotlin/org/jitsi/config/JitsiConfig.kt#L83-L91
They use this library: https://github.com/lightbend/config
We are particularly interested by: https://github.com/lightbend/config#standard-behavior
Using 'application.conf' with classpath does not seem to work.
But, specifying the file path as `-Dconfig.file=/etc/jitsi/jicofo.conf` works!

Some parameters are also set independently of lightbend hocon config.
They are seen in jicofo entrypoint:
https://github.com/jitsi/jicofo/blob/master/src/main/java/org/jitsi/jicofo/Main.java
Many of these parameters can be in fact read from the HOCON file except one: the `--secret` parameter or the `JICOFO_SECRET` env variable.
But we can see this is a deprecated thing, it has been already removed from master: https://github.com/jitsi/jicofo/commit/c9e5b50a8b4e77f8b8cb8831a4a044a53edfcf48
For now (as per v5390) we will keep `JICOFO_SECRET` environment variable but will assume no other environment variable is set
But maybe this value is deprecated: the check is still here but it is not used anymore?!

## Generate certs with prosody
```
prosodyctl cert generate auth.jitsi
prosodyctl cert generate jitsi
```

## An example prosody configuration file

https://github.com/jitsi/jitsi-meet/blob/master/doc/example-config-files/prosody.cfg.lua.example

but this one is not the one used by the [debian postinst script](https://github.com/jitsi/jitsi-meet/blob/master/debian/jitsi-meet-prosody.postinst)
instead, we should look at this one: https://github.com/jitsi/jitsi-meet/blob/master/doc/debian/jitsi-meet-prosody/prosody.cfg.lua-jvb.example

Jitsi can be configured to authenticated through tokens,
the postinst file is here: https://github.com/jitsi/jitsi-meet/blob/master/debian/jitsi-meet-tokens.postinst

## Resources to understand jitsi

  - [jicofo/debian/postinst](https://github.com/jitsi/jicofo/blob/master/debian/postinst)
  - [videobridge/debian/postinst](https://github.com/jitsi/jitsi-videobridge/blob/master/debian/postinst)
