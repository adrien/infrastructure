job "garage" {
  datacenters = ["dc1", "belair", "saturne"]
  type = "system"
  priority = 40

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "garage" {
    network {
      port "s3" { static = 3900 }
      port "rpc" { static = 3901 }
      port "web" { static = 3902 }
    }

    task "server" {
      driver = "docker"
      config {
        advertise_ipv6_address = true
        image = "lxpz/garage_amd64:v0.1.1b"
        network_mode = "host"
        volumes = [
          "/mnt/storage/garage/data:/garage/data",
          "/mnt/ssd/garage/meta:/garage/meta",
          "secrets/garage.toml:/garage/config.toml",
          "secrets/garage-ca.crt:/garage/garage-ca.crt",
          "secrets/garage.crt:/garage/garage.crt",
          "secrets/garage.key:/garage/garage.key",
        ]
      }

      template {
        data = file("../config/garage.toml")
        destination = "secrets/garage.toml"
      }

      # --- secrets ---
      template {
        data = "{{ key \"secrets/garage/garage-ca.crt\" }}" 
        destination = "secrets/garage-ca.crt"
      }
      template {
        data = "{{ key \"secrets/garage/garage.crt\" }}" 
        destination = "secrets/garage.crt"
      }
      template {
        data = "{{ key \"secrets/garage/garage.key\" }}" 
        destination = "secrets/garage.key"
      }

      resources {
        memory = 500
        cpu = 1000
      }

      service {
        tags = [
          "garage_api",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:garage.deuxfleurs.fr"
        ]
        port = 3900
        address_mode = "driver"
        name = "garage-api"
        check {
          type = "tcp"
          port = 3900
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = ["garage-rpc"]
        port = 3901
        address_mode = "driver"
        name = "garage-rpc"
        check {
          type = "tcp"
          port = 3901
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}
