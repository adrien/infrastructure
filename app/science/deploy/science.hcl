job "science" {
  datacenters = ["dc1"]
  type = "service"
  priority = 10

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "diagnet" {
    network {
      port "web_port" { to = 8000 }
    }

    task "main" {
      driver = "docker"
      config {
        image = "lesterpig/diagnet-landmark:latest"
        args = [
          "-name", "landmark-deuxfleurs",
          "-chrome", "-chrome-interval", "60m",
          "-http", ":8000"
        ]
        ports = [ "web_port" ]
      }

      resources {
        cpu = 1000
        memory = 1200
      }

      service {
        tags = [
          "diagnet",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:diagnet.science.deuxfleurs.fr;PathPrefix:/"
        ]
        port = "web_port"
        address_mode = "host"
        name = "diagnet"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

