# Folder hierarchy

- `<module>/build/<image_name>/`: folders with dockerfiles and other necessary resources for building container images
- `<module>/config/`: folder containing configuration files, referenced by deployment file
- `<module>/secrets/`: folder containing secrets, which can be synchronized with Consul using `secretmgr.py`
- `<module>/deploy/`: folder containing the HCL file(s) necessary for deploying the module
- `<module>/integration/`: folder containing files for integration testing using docker-compose

# Secret Manager `secretmgr.py`

The Secret Manager ensures that all secrets are present where they should in the cluster.

**You need access to the cluster** (SSH port forwarding) for it to find any secret on the cluster. Refer to the previous directory's [README](../README.md), at the bottom of the file.

## How to install `secretmgr.py` dependencies

```bash
### Install system dependencies first:
## On fedora

dnf install -y openldap-devel cyrus-sasl-devel
## On ubuntu
apt-get install -y libldap2-dev libsasl2-dev

### Now install the Python dependencies from requirements.txt:

## Either using a virtual environment
# (requires virtualenv python module)
python3 -m virtualenv env 
# Must be done everytime you create a new terminal window in this folder:
. env/bin/activate 
# Install the deps
pip install -r requirements.txt

## Either by installing the dependencies for your system user:
pip3 install --user -r requirements.txt
```

## How to use `secretmgr.py`

Check that all secrets are correctly deployed for app `dummy`:

```bash
./secretmgr.py check dummy
```

Generate secrets for app `dummy` if they don't already exist:

```bash
./secretmgr.py gen dummy
```

Rotate secrets for app `dummy`, overwriting existing ones (be careful, this is dangerous!):

```bash
./secretmgr.py regen dummy
```

# Upgrading one of our packaged apps to a new version

 1. Edit `docker-compose.yml`
 2. Change the `VERSION` variable to the desired version
 3. Increment the docker image tag by 1 (eg: superboum/riot:v13 -> superboum/riot:v14)
 4. Run `docker-compose build`
 5. Run `docker-compose push`
 6. Done
