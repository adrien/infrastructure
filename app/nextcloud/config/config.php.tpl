<?php
$CONFIG = array (
  'appstoreenabled' => false,
  'instanceid' => '{{ key "secrets/nextcloud/instance_id" | trimSpace }}',
  'passwordsalt' => '{{ key "secrets/nextcloud/password_salt" | trimSpace }}',
  'secret' => '{{ key "secrets/nextcloud/secret" | trimSpace }}',
  'trusted_domains' => array (
    0 => 'nextcloud.deuxfleurs.fr',
  ),
  'memcache.local' => '\\OC\\Memcache\\APCu',

  'objectstore' => array(
    'class' => '\\OC\\Files\\ObjectStore\\S3',
    'arguments' => array(
          'bucket' => 'nextcloud',
          'autocreate' => false,
          'key'    => '{{ key "secrets/nextcloud/garage_access_key" | trimSpace }}',
          'secret' => '{{ key "secrets/nextcloud/garage_secret_key" | trimSpace }}',
          'hostname' => 'garage.deuxfleurs.fr',
          'port' => 443,
          'use_ssl' => true,
          'region' => 'garage',
          // required for some non Amazon S3 implementations
          'use_path_style' => true
    ),
  ),

  'dbtype' => 'pgsql',
  'dbhost' => 'psql-proxy.service.2.cluster.deuxfleurs.fr',
  'dbname' => 'nextcloud',
  'dbtableprefix' => 'nc_',
  'dbuser' => '{{ key "secrets/nextcloud/db_user" | trimSpace }}',
  'dbpassword' => '{{ key "secrets/nextcloud/db_pass" | trimSpace }}',

  'default_language' => 'fr',
  'default_locale' => 'fr_FR',

  'mail_domain' => 'deuxfleurs.fr',
  'mail_from_address' => 'nextcloud@deuxfleurs.fr',
  // TODO SMTP CONFIG

  // TODO REDIS CACHE

  'version' => '19.0.0.12',
  'overwrite.cli.url' => 'https://nextcloud.deuxfleurs.fr',

  'installed' => true,
);

