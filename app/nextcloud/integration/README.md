Install Owncloud CLI: 

php ./occ \
  --no-interaction \
  --verbose \
  maintenance:install \
  --database pgsql \
  --database-name nextcloud \
  --database-host postgres \
  --database-user nextcloud \
  --database-pass nextcloud \
  --admin-user nextcloud \
  --admin-pass nextcloud \
  --admin-email coucou@deuxfleurs.fr

Official image entrypoint:

https://github.com/nextcloud/docker/blob/master/20.0/fpm/entrypoint.sh


