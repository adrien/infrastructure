job "nextcloud" {
  datacenters = ["dc1", "belair"]
  type = "service"
  priority = 40

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "nextcloud" {
    count = 1

    network {
      port "web_port" {
        to = 80
      }
    }

    task "nextcloud" {
      driver = "docker"
      config {
        image = "lxpz/deuxfleurs_nextcloud_amd64:8"
        ports = [ "web_port" ]
        volumes = [
          "secrets/config.php:/var/www/html/config/config.php"
        ]
      }

      template {
        data = file("../config/config.php.tpl")
        destination = "secrets/config.php"
      }

      resources {
        memory = 1000
        cpu = 2000
      }

      service {
        name = "nextcloud"
        tags = [
          "nextcloud",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:nextcloud.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

