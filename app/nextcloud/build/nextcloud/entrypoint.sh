#!/bin/sh

set -xe

chown www-data:www-data /var/www/html/config/config.php
touch /var/www/html/data/.ocdata

exec apachectl -DFOREGROUND
