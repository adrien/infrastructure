job "directory" {
  datacenters = ["dc1"]
  type = "service"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "bottin" {
    count = 1

    network {
      port "ldap_port" {
        static = 389
        to = 389
      }
    }

    task "bottin" {
      driver = "docker"
      config {
        image = "lxpz/bottin_amd64:20"
        network_mode = "host"
        readonly_rootfs = true
        ports = [ "ldap_port" ]
        volumes = [
          "secrets/config.json:/config.json"
        ]
      }

      resources {
        memory = 100
      }

      template {
        data = file("../config/bottin/config.json")
        destination = "secrets/config.json"
      }

      service {
        tags = ["bottin"]
        port = "ldap_port"
        address_mode = "host"
        name = "bottin2"
        check {
          type = "tcp"
          port = "ldap_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }

  group "guichet" {
    count = 1

    network {
      port "web_port" { to = 9991 }
    }

    task "guichet" {
      driver = "docker"
      config {
        image = "lxpz/guichet_amd64:10"
        readonly_rootfs = true
        ports = [ "web_port" ]
        volumes = [
          "secrets/config.json:/config.json"
        ]
      }

      template {
        data = file("../config/guichet/config.json.tpl")
        destination = "secrets/config.json"
      }

      resources {
        memory = 200
      }

      service {
        name = "guichet"
        tags = [
          "guichet",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:guichet.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

