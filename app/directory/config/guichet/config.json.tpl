{
  "http_bind_addr": ":9991",
  "ldap_server_addr": "ldap://bottin2.service.2.cluster.deuxfleurs.fr:389",

  "base_dn": "dc=deuxfleurs,dc=fr",
  "user_base_dn": "ou=users,dc=deuxfleurs,dc=fr",
  "user_name_attr": "cn",
  "group_base_dn": "ou=groups,dc=deuxfleurs,dc=fr",
  "group_name_attr": "cn",

  "invitation_base_dn": "ou=invitations,dc=deuxfleurs,dc=fr",
  "invitation_name_attr": "cn",
  "invited_mail_format": "{}@deuxfleurs.fr",
  "invited_auto_groups": [
    "cn=email,ou=groups,dc=deuxfleurs,dc=fr",
    "cn=seafile,ou=groups,dc=deuxfleurs,dc=fr",
    "cn=nextcloud,ou=groups,dc=deuxfleurs,dc=fr"
  ],

  "web_address": "https://guichet.deuxfleurs.fr",
  "mail_from": "coucou@deuxfleurs.fr",
  "smtp_server": "adnab.me:25",
  "smtp_username": "{{ key "secrets/directory/guichet/smtp_user" | trimSpace }}",
  "smtp_password": "{{ key "secrets/directory/guichet/smtp_pass" | trimSpace }}",

  "admin_account": "cn=admin,dc=deuxfleurs,dc=fr",
  "group_can_admin": "cn=admin,ou=groups,dc=deuxfleurs,dc=fr",
  "group_can_invite": "cn=asso_deuxfleurs,ou=groups,dc=deuxfleurs,dc=fr"
}

