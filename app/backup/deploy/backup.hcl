job "backup_periodic" {
	datacenters = ["dc1"]

	type = "batch"

	periodic {
	  // Launch every hour
	  cron = "0 * * * * *"

	  // Do not allow overlapping runs.
	  prohibit_overlap = true
	}

	task "backup-consul" {
		driver = "docker"

			config {
				image = "lxpz/backup_consul:12"
				volumes = [
					"secrets/id_ed25519:/root/.ssh/id_ed25519",
					"secrets/id_ed25519.pub:/root/.ssh/id_ed25519.pub",
					"secrets/known_hosts:/root/.ssh/known_hosts"
				]
				network_mode = "host"
			}

		env {
			CONSUL_HTTP_ADDR = "http://consul.service.2.cluster.deuxfleurs.fr:8500"
		}

		template {
			data = <<EOH
TARGET_SSH_USER={{ key "secrets/backup/target_ssh_user" }}
TARGET_SSH_PORT={{ key "secrets/backup/target_ssh_port" }}
TARGET_SSH_HOST={{ key "secrets/backup/target_ssh_host" }}
TARGET_SSH_DIR={{ key "secrets/backup/target_ssh_dir" }}
EOH

			destination = "secrets/env_vars"
			env = true
		}

		template {
			data = "{{ key \"secrets/backup/id_ed25519\" }}"
			destination = "secrets/id_ed25519"
		}
		template {
			data = "{{ key \"secrets/backup/id_ed25519.pub\" }}"
			destination = "secrets/id_ed25519.pub"
		}
		template {
			data = "{{ key \"secrets/backup/target_ssh_fingerprint\" }}"
			destination = "secrets/known_hosts"
		}

		resources {
			memory = 200
		}

		restart {
			attempts = 2
			interval = "30m"
			delay = "15s"
			mode = "fail"
		}
	}
}
