Spawn container:

```bash
docker run -t -i superboum/arm32v7_postgres:v6 
# OR
docker run -t -i superboum/amd64_postgres:v1 
```


Init with:

```
stolonctl \
  --cluster-name pissenlit \
  --store-backend=consul \
  --store-endpoints http://consul.service.2.cluster.deuxfleurs.fr:8500 \
  init \
  '{ "initMode": "new", "pgHBA": [ "host all postgres all md5", "host replication replicator all md5", "host all all all ldap ldapserver=bottin.service.2.cluster.deuxfleurs.fr ldapbasedn=\"ou=users,dc=deuxfleurs, dc=fr\" ldapbinddn=\"<bind_dn>\" ldapbindpasswd=\"<bind_pwd>\" ldapsearchattribute=\"cn\"" ] }'

```

Then set appropriate permission on host:

```
chown -R 102:102 /mnt/storage/postgres/
```

(102 is the id of the postgres user used in Docker)
It might be improved by staying with root, then chmoding in an entrypoint and finally switching to user 102 before executing user's command.
Moreover it would enable the usage of the user namespace that shift the UIDs.



## Upgrading the cluster

To retreive the current stolon config:

```
stolonctl spec --cluster-name pissenlit --store-backend consul --store-endpoints http://consul.service.2.cluster.deuxfleurs.fr:8500
```

The important part for the LDAP:

```
{
	"pgHBA": [
		"host all postgres all md5",
		"host replication replicator all md5",
		"host all all all ldap ldapserver=bottin.service.2.cluster.deuxfleurs.fr ldapbasedn=\"ou=users,dc=deuxfleurs,dc=fr\" ldapbinddn=\"cn=admin,dc=deuxfleurs,dc=fr\" ldapbindpasswd=\"<REDACTED>\" ldapsearchattribute=\"cn\""
	]
}
```

Once a patch is writen:

```
stolonctl --cluster-name pissenlit --store-backend consul --store-endpoints http://consul.service.2.cluster.deuxfleurs.fr:8500 update --patch -f /tmp/patch.json
```

## Log

- 2020-12-18 Activate pg\_rewind in stolon

```
stolonctl --cluster-name pissenlit --store-backend consul --store-endpoints http://consul.service.2.cluster.deuxfleurs.fr:8500 update --patch '{ "usePgrewind" : true }'
```
