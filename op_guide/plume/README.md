## Creating a new Plume user

  1. Bind nomad on your machine with SSH (check the README file at the root of this repo)
  2. Go to http://127.0.0.1:4646
  3. Select `plume` -> click `exec` button (top right)
  4. Select `plume` on the left panel
  5. Press `enter` to get a bash shell
  6. Run:

```bash
plm users new \
  --username alice \
  --display-name Alice \
  --bio Just an internet user \
  --email alice@example.com \
  --password s3cr3t
```

That's all folks, now you can use your new account at https://plume.deuxfleurs.fr
 
