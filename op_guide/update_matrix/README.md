How to update Matrix?
=====================

## 1. Build the new containers

Often, I update Riot Web and Synapse at the same time.


* Open `app/docker-compose.yml` and locate `riot` (the Element Web service) and `synapse` (the Matrix Synapse server). There are two things you need to do for each service:

    * Set the `VERSION` argument to the target service version (e.g. `1.26.0` for Synapse). This argument is then used to template the Dockerfile. 

    The `VERSION` value should match a github release, the link to the corresponding release page is put as a comment next to the variable in the compose file;

    * Tag the image with a new incremented version tag. For example: `superboum/amd64_riotweb:v17` will become `superboum/amd64_riotweb:v18`.

    We use the docker hub to store our images. So, if you are not `superboum` you must change the name with your own handle, eg. `john/amd64_riotweb:v18`. This requires that you registered an account (named `john`) on https://hub.docker.com.


So, from now we expect you have:

* changed the `VERSION` value and `image` name/tag of `riot`
* changed the `VERSION` value and `image` name/tag of `synapse`

From the `/app` folder, you can now simply build and push the new images:

```bash
docker-compose build riot synapse
```

And then send them to the docker hub:

```
docker-compose push riot synapse
```

Don't forget to commit and push your changes before doing anything else!

## 2. Deploy the new containers

Now, we will edit the deployment file `app/im/deploy/im.hcl`.

Find where the image is defined in the file, for example Element-web will look like that:


```hcl
  group "riotweb" {
    count = 1

    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_riotweb:v17"
        port_map {
          web_port = 8043
        }
```

And replace the `image =` entry with its new version created above.
Do the same thing for the `synapse` service.

Now, you need a way to access the cluster to deploy this file.
To do this, you must bind nomad on your machine through a SSH tunnel.
Check the end of [the parent `README.md`](../README.md) to do it.
If you have access to the Nomad web UI when entering http://127.0.0.1:4646
you are ready to go.

You must have installed the Nomad command line tool on your machine (also explained in [the parent `README.md`](../README.md)).

Now, on your machine and from the `app/im/deploy` folder, you must be able to run:

```
nomad plan im.hcl
```

Check that the proposed diff corresponds to what you have in mind.
If it seems OK, just copy paste the `nomad job run ... im.hcl` command proposed as part of the output of the `nomad plan` command.

From now, it will take around ~2 minutes to deploy the new images.
You can follow the deployment from the Nomad UI.
Bear in mind that, once the deployment is done on Nomad, you may still need to wait some minutes that Traefik refreshes its configuration.

If everythings worked as intended, you can commit and push your deployment file.

If something went wrong, you must rollback your deployment.

  1. First, find a working deployment with [nomad job history](https://www.nomadproject.io/docs/commands/job/history)
  2. Revert to this deployment with [nomad job revert](https://www.nomadproject.io/docs/commands/job/revert)

Now, if the deployment failed, you should probably investigate what went wrong offline.
I built a test stack with docker-compose in `app/<service>/integration` that should help you out (for now, test suites are only written for plume and jitsi).


